#pragma once
#include "Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string name);
	Customer();
	~Customer();
	double totalSum();//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set

	std::set<Item> getItems();
	std::string getName();

private:
	std::string _name;
	std::set<Item> _items;


};
