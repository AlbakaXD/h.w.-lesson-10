#include"Customer.h"
#include<map>

#define NEW_CUSTOMER 1
#define UPDATE 2
#define PRINT_FATEST 3
#define EXIT 4

int main()
{

	std::map<std::string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	int mainOption = 0, optionOfProduct = -1, optionOfSecond = 0;
	std::string name = "";
	while (mainOption != 4)
	{
		std::cout << "Welcome to MagshiMart!" << std::endl;
		std::cout << "1. to sign as customer and buy items"<< std::endl;
		std::cout << "2. to update existing customer's items"<< std::endl;
		std::cout << "3. to print the customer who pays the most"<< std::endl;
		std::cout << "4. to exit"<< std::endl;
		std::wcin >> mainOption;
		if (mainOption == NEW_CUSTOMER)
		{
			std::cout << std::endl;
			std::cout << "please enter your name" << std::endl;
			std::cin >> name;
			if (abcCustomers.find(name) != abcCustomers.end())
			{
				std::cout << "ERROR: Name is already taken" << std::endl;
			}
			else
			{
				abcCustomers[name] = Customer(name);
			}

			while (optionOfProduct != 0)
			{
				std::cout << "The items you can buy are : (0 to exit)" << std::endl;
				std::cout << "1. milk price : 5.30" << std::endl;
				std::cout << "2. cookies price : 12.60" << std::endl;
				std::cout << "3. bread price : 8.90" << std::endl;
				std::cout << "4. chocolate price : 7.00" << std::endl;
				std::cout << "5. cheese price : 15.30" << std::endl;
				std::cout << "6. rice price : 6.20" << std::endl;
				std::cout << "7. fish price : 31.65" << std::endl;
				std::cout << "8. chicken price : 25.99" << std::endl;
				std::cout << "9. cucumber price : 1.21" << std::endl;
				std::cout << "10. tomato price : 2.32" << std::endl;

				std::cout << "What item would you like to buy ? Input: ";
				std::cin >> optionOfProduct;
				if (optionOfProduct == 1)
				{
					abcCustomers[name].addItem(itemList[0]);
				}
				else if(optionOfProduct == 2)
				{
					abcCustomers[name].addItem(itemList[1]);
				}
				else if (optionOfProduct == 3)
				{
					abcCustomers[name].addItem(itemList[2]);
				}
				else if (optionOfProduct == 4)
				{
					abcCustomers[name].addItem(itemList[3]);
				}
				else if (optionOfProduct == 5)
				{
					abcCustomers[name].addItem(itemList[4]);
				}
				else if (optionOfProduct == 6)
				{
					abcCustomers[name].addItem(itemList[5]);
				}
				else if (optionOfProduct == 7)
				{
					abcCustomers[name].addItem(itemList[6]);
				}
				else if (optionOfProduct == 8)
				{
					abcCustomers[name].addItem(itemList[7]);
				}
				else if (optionOfProduct == 9)
				{
					abcCustomers[name].addItem(itemList[8]);
				}
				else if (optionOfProduct == 10)
				{
					abcCustomers[name].addItem(itemList[9]);
				}
				else
				{
					std::cout << "ERROR: Invalid option" << std::endl;
				}
				std::cout << std::endl;

			}
		}
		else if (mainOption == UPDATE)
		{
			std::cout << "Please Enter your name: " << std::endl;
			std::cin >> name;
			if (abcCustomers.find(name) == abcCustomers.end())
			{
				std::cout << "ERROR: Customer does not exist" << std::endl;
			}
			else
			{
				while (optionOfSecond != 3)
				{
					std::cout << "1. Add Item" << std::endl;
					std::cout << "2. Remove item" << std::endl;
					std::cout << "3. Back to menu" << std::endl;
					std::cin >> optionOfSecond;
					if (optionOfSecond == 1)
					{
						std::cout << "The items you can buy are :" << std::endl;
						std::cout << "1. milk price : 5.30" << std::endl;
						std::cout << "2. cookies price : 12.60" << std::endl;
						std::cout << "3. bread price : 8.90" << std::endl;
						std::cout << "4. chocolate price : 7.00" << std::endl;
						std::cout << "5. cheese price : 15.30" << std::endl;
						std::cout << "6. rice price : 6.20" << std::endl;
						std::cout << "7. fish price : 31.65" << std::endl;
						std::cout << "8. chicken price : 25.99" << std::endl;
						std::cout << "9. cucumber price : 1.21" << std::endl;
						std::cout << "10. tomato price : 2.32" << std::endl;

						std::cout << "What item would you like to buy ? Input: ";
						std::cin >> optionOfProduct;
						if (optionOfProduct == 1)
						{
							abcCustomers[name].addItem(itemList[0]);
						}
						else if (optionOfProduct == 2)
						{
							abcCustomers[name].addItem(itemList[1]);
						}
						else if (optionOfProduct == 3)
						{
							abcCustomers[name].addItem(itemList[2]);
						}
						else if (optionOfProduct == 4)
						{
							abcCustomers[name].addItem(itemList[3]);
						}
						else if (optionOfProduct == 5)
						{
							abcCustomers[name].addItem(itemList[4]);
						}
						else if (optionOfProduct == 6)
						{
							abcCustomers[name].addItem(itemList[5]);
						}
						else if (optionOfProduct == 7)
						{
							abcCustomers[name].addItem(itemList[6]);
						}
						else if (optionOfProduct == 8)
						{
							abcCustomers[name].addItem(itemList[7]);
						}
						else if (optionOfProduct == 9)
						{
							abcCustomers[name].addItem(itemList[8]);
						}
						else if (optionOfProduct == 10)
						{
							abcCustomers[name].addItem(itemList[9]);
						}
						else
						{
							std::cout << "ERROR: Invalid option" << std::endl;
						}
						std::cout << std::endl;

					}
					else if (optionOfSecond == 2)
					{
						std::cout << "The items you can remove are :" << std::endl;
						std::cout << "1. milk price : 5.30" << std::endl;
						std::cout << "2. cookies price : 12.60" << std::endl;
						std::cout << "3. bread price : 8.90" << std::endl;
						std::cout << "4. chocolate price : 7.00" << std::endl;
						std::cout << "5. cheese price : 15.30" << std::endl;
						std::cout << "6. rice price : 6.20" << std::endl;
						std::cout << "7. fish price : 31.65" << std::endl;
						std::cout << "8. chicken price : 25.99" << std::endl;
						std::cout << "9. cucumber price : 1.21" << std::endl;
						std::cout << "10. tomato price : 2.32" << std::endl;

						std::cout << "What item would you like to remove ? Input: ";
						std::cin >> optionOfProduct;
						if (optionOfProduct == 1)
						{
							abcCustomers[name].removeItem(itemList[0]);
						}
						else if (optionOfProduct == 2)
						{
							abcCustomers[name].removeItem(itemList[1]);
						}
						else if (optionOfProduct == 3)
						{
							abcCustomers[name].removeItem(itemList[2]);
						}
						else if (optionOfProduct == 4)
						{
							abcCustomers[name].removeItem(itemList[3]);
						}
						else if (optionOfProduct == 5)
						{
							abcCustomers[name].removeItem(itemList[4]);
						}
						else if (optionOfProduct == 6)
						{
							abcCustomers[name].removeItem(itemList[5]);
						}
						else if (optionOfProduct == 7)
						{
							abcCustomers[name].removeItem(itemList[6]);
						}
						else if (optionOfProduct == 8)
						{
							abcCustomers[name].removeItem(itemList[7]);
						}
						else if (optionOfProduct == 9)
						{
							abcCustomers[name].removeItem(itemList[8]);
						}
						else if (optionOfProduct == 10)
						{
							abcCustomers[name].removeItem(itemList[9]);
						}
						else
						{
							std::cout << "ERROR: Invalid option" << std::endl;
						}
						std::cout << std::endl;

					}
					else
					{
						std::cout << "ERROR: Invalid option" << std::endl;
					}
				}
			}
		}
		else if (mainOption == PRINT_FATEST)
		{
			if (abcCustomers.begin() == abcCustomers.end())
			{
				std::cout << "ERROR: No customers" << std::endl;
			}
			else
			{
				std::map<std::string, Customer>::iterator fattest = abcCustomers.begin();

				for (std::map<std::string, Customer>::iterator i = abcCustomers.begin(); i != abcCustomers.end(); i++)
				{
					if (i->second.totalSum() > fattest->second.totalSum())
					{
						fattest = i;
					}
				}
				std::cout << "Name: " << fattest->first << std::endl;
				std::cout << "Price: " << fattest->second.totalSum() << std::endl;

			}
		}
		else if (mainOption == EXIT)
		{
			std::cout << "Thank you for using our program" << std::endl;
		}
		else
		{
			std::cout << "ERROR: Invalid option" << std::endl;
		}
	}


	return 0;
}