#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice()
{
	return this->_unitPrice * this->_count;
}

bool Item::operator<(const Item& other) const
{
	if (this->_serialNumber < other._serialNumber)
	{
		return true;
	}
	return false;
}

bool Item::operator>(const Item& other) const
{
	if (this->_serialNumber > other._serialNumber)
	{
		return true;
	}
	return false;
}

bool Item::operator==(const Item& other) const
{
	if (this->_serialNumber == other._serialNumber)
	{
		return true;
	}
	return false;
}

std::string Item::getName() const
{
	return this->_name;
}

std::string Item::getSerial() const
{
	return this->_serialNumber;
}

double Item::getPrice() const
{
	return this->_unitPrice;
}

int Item::getCount() const
{
	return this->_count;
}

void Item::setCount(int newCount)
{
	this->_count = newCount;
}
