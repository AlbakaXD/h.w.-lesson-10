#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::Customer()
{
	this->_name = "";
}

Customer::~Customer()
{
}

double Customer::totalSum()
{
	double sum = 0;
	for (std::set<Item>::iterator i = this->_items.begin(); i!=this->_items.end();i++)
	{
		sum = sum + i->getPrice();
	}

	return sum;
}

void Customer::addItem(Item item)
{
	int count = 0;
	if (this->_items.find(item) == this->_items.end())
	{
		this->_items.insert(item);
	}
	else
	{
		std::set<Item>::iterator thingy = this->_items.find(item);
		count = thingy->getCount();
		this->_items.erase(item);
		count = count + 1;
		item.setCount(count);
		this->_items.insert(item);
	}
}

void Customer::removeItem(Item item)
{
	int count = 0;
	if (this->_items.find(item) == this->_items.end())
	{
		std::cout << "You didn't take this item!" << std::endl;
	}
	else
	{
		std::set<Item>::iterator thingy = this->_items.find(item);
		count = thingy->getCount();
		this->_items.erase(item);
		count = count - 1;
		if (count != 0)
		{
			item.setCount(count);
			this->_items.insert(item);
		}
	}
}

std::set<Item> Customer::getItems()
{
	return this->_items;
}

std::string Customer::getName()
{
	return this->_name;
}
